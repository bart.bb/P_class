import numpy as np

import numba
from numba import jit
from numba import prange

import sys

from scipy.fft import fftn
from scipy.fft import fftshift
from scipy.interpolate import CubicSpline
from scipy.special import sph_harm

import math

import matplotlib.pyplot as plt


#Small tool to compute the power spectrum of density fields
#It can consider simulation snapshots or survey data

class FieldClass():
    
    def __init__(self,h0,Om0,Ode0,cross=False):
        
        #Loaded with the field
        self._field_loaded=False
        self._Ngrid=None
        self._Ngrid3=None
        self._field1=None
        self._field2=None
        self._boxsize=None
        self._MAS=None
        self._is_survey=None
        self._is_cross=bool(cross)
        
        #Only if lightcone data
        self._n1_bin=None
        self._r1_bin=None
        self._window1=None

        self._n2_bin=None
        self._r2_bin=None
        self._window2=None
        
        #Computed with the field
        self._PSN=None

        #Set with the dft
        self._dft_computed=False
        self._field1_dft=None
        self._field2_dft=None
        self._poles=None
        self._F1_l=None
        self._F2_l=None
        self._dft_k=None
        
        
        #Cosmological parameters
        self._h0=h0
        self._Om0=Om0
        self._Ode0=Ode0
        
        #Constants
        self._c=299792.458
        self._G=6.6743e-11
        
    
    #Private methods--------------------------------------------------------------------------- 
      
        
    def __repr__(self):
     
        return 'Field analysis tool'
    
    
    #Routine to load the field interpolated on a mesh
    def __load_field(self,field : np.ndarray,Ngrid : np.uintc,boxsize : np.single,cshape : str,is_survey : bool):
    
        #Before loading anything we check all the conditions
        
        self.__field_check(field,Ngrid)
        self.__MAS_check(cshape)
    

        if self._field_loaded==True:
            print('Error! Field/s already loaded.',file=sys.stderr)
            sys.exit()

        #If we are planning to cross correlate, we allow two fields to be loaded, otherwise only one

        if (self._is_cross==True):
            
            if (self._field1 is None): 
        
                self._Ngrid=np.uintc(Ngrid)
                self._Ngrid3=np.ulonglong(Ngrid**3)
                self._boxsize=np.single(boxsize)
                self._MAS=cshape
                self._is_survey=bool(is_survey)

                self._field1=np.copy(field).reshape(self._Ngrid3)
                

            elif (self._field2 is None):

                #We check that the field has the same number of cells
                self.__field_check(field,self._Ngrid)

                self._field2=np.copy(field).reshape(self._Ngrid3)

                self._field_loaded=True

        else:
            
            self._Ngrid=np.uintc(Ngrid)
            self._Ngrid3=np.ulonglong(Ngrid**3)
            self._boxsize=np.single(boxsize)
            self._MAS=cshape
            self._is_survey=bool(is_survey)

            self._field1=np.copy(field).reshape(self._Ngrid3)

            self._field_loaded=True

        
    #Build the density field from the particles in a simulation box
    def __build_n_from_particles(self,pos : np.ndarray,Ngrid : np.uintc,boxsize : np.double,cshape : str,
                              centered : bool):
        
        Ngrid3=Ngrid**3
        
        #To avoid common mistakes, check the sanity of the positions array

        if len(pos)!=3:
            
            print('Positions array in the wrong form, try using pos.T instead',file=sys.stderr)
            sys.exit()
    
        Npart=len(pos[0])
        
        if len(pos[1])!=Npart or len(pos[2])!=Npart:
            
            print('Positions array not consistent',file=sys.stderr)
            sys.exit()

            
        #Check if all particles are inside the box
    
        if centered == True:
            
            if np.any(np.abs(pos[0])>boxsize/2.) or np.any(np.abs(pos[1])>boxsize/2.) or \
            np.any(np.abs(pos[2])>boxsize/2):

                print('Particles outside the box',file=sys.stderr)
                sys.exit()
            
        else:
       
            if np.any(pos[0]>boxsize) or np.any(pos[1]>boxsize) or \
            np.any(pos[2]>boxsize):
            
                print('Particles outside the box',file=sys.stderr)
                sys.exit()       
                
            if np.any(pos[0]<0) or np.any(pos[1]<0) or np.any(pos[2]<0):
                
                print('Particles outside the box',file=sys.stderr)
                sys.exit()    
                

        self.__MAS_check(cshape)

        if centered == True:
            x=pos[0].astype(np.double)+boxsize/2.
            y=pos[1].astype(np.double)+boxsize/2.
            z=pos[2].astype(np.double)+boxsize/2.
        else:
            x=pos[0].astype(np.double)
            y=pos[1].astype(np.double)
            z=pos[2].astype(np.double)
            
        #Compute the density
        n=self.__get_n(x,y,z,Npart,Ngrid,boxsize,cshape)
        
        n_bkg=np.mean(n)
        
        #Compute the overdensity
        d=(n[:]-n_bkg)/n_bkg
        
        #Load the density as a field    
        self.__load_field(d,Ngrid,boxsize,cshape,False)


    #Compute the binned radial density from the density mesh
    def __compute_nbin_masked(self,dens : np.ndarray,Ngrid : np.uintc,boxsize : np.double,rmin : np.single,rmax : np.single,Nbins : np.intc,mask = None):

        dr=(rmax-rmin)/(Nbins-1)

        if rmin-dr/2.<=0.:
            print('The inner radius of the shell is too close to 0. Choose a finer grid or use a bigger radius.', file=sys.stderr)
            sys.exit()

        Ngrid3=Ngrid**3
        
        if dens.shape==(Ngrid,Ngrid,Ngrid):
            n=dens.reshape(Ngrid3)
        elif len(dens)==Ngrid3:
            n=np.copy(dens)
        else:
            print('The input is not in a recognizable shape.', file=sys.stderr)
            sys.exit()


        #If the mask is not present, use a fullsky one
        if mask is None:
            M=lambda pos_t: np.ones(pos_t[0].size,dtype=bool)
        else:
            M=mask

        r=np.linspace(rmin-dr/2.,rmax+dr/2.,Nbins+1)

        #Retrieve the position of each cell

        pos_c=koor2pos(np.arange(0,Ngrid3,1),boxsize,Ngrid,centered=True)
        dist_c=koorDist(np.arange(0,Ngrid3,1),boxsize,Ngrid)

        nShell=np.zeros(Nbins,dtype=np.double)
        rShell=np.zeros(Nbins,dtype=np.double)

        for i in range(Nbins):     

            idx=(r[i]<=dist_c)&(dist_c<r[i+1])&(M(pos_c)==True)

            if np.all(idx==False):
                nShell[i]=0
            else:
                nShell[i]=np.average(n[idx])

            rShell[i]=(r[i]+r[i+1])/2.
    
            
        return rShell,nShell
        
    
    #Compute the weighted density field, following the FKP approach
    def __build_FKP_from_particles(self,pos : np.ndarray,Ngrid : np.uintc,boxsize : np.double,cshape : str,weight,
        rmin : np.double,rmax : np.double,Nbins : np.uintc,mask = None):
        
        Ngrid3=Ngrid**3
        
        #Positions sanity check
        
        if len(pos)!=3:
            
            print('positions array in the wrong form, try using pos.T instead',file=sys.stderr)
            sys.exit()
    
        Npart=len(pos[0])
        
        if len(pos[1])!=Npart or len(pos[2])!=Npart:
            
            print('positions array not consistent',file=sys.stderr)
            sys.exit()
            
        #Select the particles inside the box

        msk=(np.abs(pos[0])<=boxsize/2.)&(np.abs(pos[1])<=boxsize/2.)&(np.abs(pos[2])<=boxsize/2.)

        self.__MAS_check(cshape)
        
        x=pos[0][msk].astype(np.double)+boxsize/2.
        y=pos[1][msk].astype(np.double)+boxsize/2.
        z=pos[2][msk].astype(np.double)+boxsize/2.
        
        del msk
            
        #Compute the density
        n=self.__get_n(x,y,z,Npart,Ngrid,boxsize,cshape)

        #If the mask is not present, use a fullsky one
        if mask is None:
            M=lambda pos_t: np.ones(pos_t[0].size,dtype=bool)
        else:
            M=mask

        r_bin,n_bin=self.__compute_nbin_masked(n,Ngrid,boxsize,rmin,rmax,Nbins,M)
        
        #Compute the window function at each grid cell

        pos_c=koor2pos(np.arange(0,Ngrid3,1),boxsize,Ngrid,centered=True)
        dist_c=koorDist(np.arange(0,Ngrid3,1),boxsize,Ngrid)

        nmean_fun=self.__build_spline(r_bin,n_bin)
        nmean=nmean_fun(dist_c)

        #Compute the window function in each cell

        W=np.where(M(pos_c)==True,weight(nmean)*nmean,0)
        Norm_W=np.sqrt(np.sum(W**2)*(boxsize/Ngrid)**3)
        W=W/Norm_W

        n=n.reshape(Ngrid3)

        #Get FPK estimator
        F=np.divide((n-nmean)*W, nmean, out=np.zeros_like(n), where=np.where((rmin<=dist_c) & (dist_c<=rmax),True,False))   #Sets F to 0 outside the shell
        F.reshape(Ngrid3)

        #Compute the shot noise
        PSN=np.sum((weight(nmean)**2*nmean)[M(pos_c)==True])/np.sum((weight(nmean)**2*nmean**2)[M(pos_c)==True])

        self._PSN=PSN

        #Load the density as a field    
        self.__load_field(F,Ngrid,boxsize,cshape,True)

            
    #Routine to ease the computation of the fast fourier transform
    def __compute_fft(self,field : np.ndarray,Nfft : np.uintc,fftbox : np.single,workers : int = 1):
    
        self.__field_check(field,Nfft)
        
        f=field.reshape(Nfft,Nfft,Nfft)
        
        Nfft3=Nfft**3
        
        f_fft=fftn(f,s=(Nfft,Nfft,Nfft),axes=(0,1,2),workers=workers)
        f_fft=fftshift(f_fft)
    
        koor=np.arange(0,Nfft3,dtype=int)
        kx,ky,kz=koor2grid(koor,Nfft,centered=True)
        kx=kx*2*math.pi/fftbox
        ky=ky*2*math.pi/fftbox
        kz=kz*2*math.pi/fftbox

        f_fft=f_fft.reshape(Nfft3)
        
        return f_fft,kx,ky,kz
    

    #Compensation technique to deconvolve the MAS window from the fourier-space density
    #IMPORTANT: Only used for the power spectrum monopole for now        
    def __compensate_field(self,field_dft : np.ndarray,k : np.ndarray,Nfft : np.uintc,boxsize : np.single,
                           cshape : str,aliasing : bool):
        
        self.__field_check(field_dft,Nfft)
        self.__MAS_check(cshape)
        #Check k array
        
        if aliasing == False:
            field_dft_comp=field_dft/self.__MAS_ft(k,Nfft,boxsize,cshape)
        else:
            field_dft_comp=field_dft/self.__MAS_aliasing(k,Nfft,boxsize,cshape)
        
        return field_dft_comp
        
    #Returns the Fourier transform of the MAS window
    def __MAS_ft(self,k : np.ndarray,Nfft : np.uintc,boxsize : np.single,cshape : str):
        
        self.__MAS_check(cshape)
        #Check k array
        
        #Nyquist frequency
        kN=math.pi*Nfft/boxsize
        
        factor=np.sinc(k.T[0]/(2*kN))*np.sinc(k.T[1]/(2*kN))*np.sinc(k.T[2]/(2*kN))
        
        if cshape == 'NGP':
            return factor
        elif cshape == 'CIC':
            return factor**2
        else:
            print("MAS not available!", file=sys.stderr)
            sys.exit()

    #Factor to correct for aliasing in the power spectrum
    def __MAS_aliasing(self,k : np.ndarray,Nfft : np.uintc,boxsize : np.single,cshape : str):
        
        self.__MAS_check(cshape)
        #Check k array
        
        #Nyquist frequency
        kN=math.pi*Nfft/boxsize

        wind_const = math.pi/(2*kN)
        
        if cshape == 'NGP':
            return np.ones_like(k.T[0])
        elif cshape == 'CIC':
            return ((1-2./3*np.sin(k.T[0]*wind_const)**2)*\
                (1-2./3*np.sin(k.T[1]*wind_const)**2)*(1-2./3*np.sin(k.T[2]*wind_const)**2))
        else:
            print("MAS not available!", file=sys.stderr)
            sys.exit()


    #Utilities
    
    #Checks the consistency of the loaded field
    @staticmethod
    def __field_check(field : np.ndarray,Ngrid : np.uintc):

        if field.shape==(Ngrid,Ngrid,Ngrid):
            pass
        elif len(field)==Ngrid**3:
            pass
        else:
            print('The input is not in a recognizable shape.', file=sys.stderr)
            sys.exit()
            
    #Checks that the MAS used is implemented
    def __MAS_check(self,cshape):
        
        if cshape == 'NGP':
            pass
        elif cshape == 'CIC':
            pass
        else :
            print('Cell shape not known.', file=sys.stderr)
            sys.exit()

    
    #Computes the interpolated number density from a set of positions
    @staticmethod
    @jit(nopython=True,parallel=True)
    def __get_n(x : np.array,y : np.array,z : np.array,Npart : np.ulonglong,Ngrid : np.uintc,
                   boxsize : np.single,cshape : str):
        
        Ngrid3=Ngrid**3
        
        n=np.zeros(shape=(Ngrid,Ngrid,Ngrid),dtype=np.double)
            
        if cshape=="NGP":
            
            for j in prange(Npart):
                
                a,b,c=pos2grid(x[j],y[j],z[j],boxsize,Ngrid)
                n[a,b,c]+=1
        
        
        elif cshape=="CIC":
        
            for j in prange(Npart):
                
                a,b,c=pos2grid(x[j],y[j],z[j],boxsize,Ngrid)
                
                for d in [-1,0,1]:
                    
                    for e in [-1,0,1]:
                        
                        for f in [-1,0,1]:
                            
                            n[periGrid(a+d,Ngrid),periGrid(b+e,Ngrid),periGrid(c+f,Ngrid)]+=\
                            CIC_weight(x[j],y[j],z[j],a+d,b+e,c+f,Ngrid,boxsize)
            
        else:

            pass
            
            
        n/=(boxsize/Ngrid)**3
        
        
        return n

    
    #Multipoles methods

    #Compute the monopole of the cross-correlation of f1 and f2
    def __compute_monopole(self,f1_fft : np.ndarray,f2_fft : np.ndarray,k : np.ndarray,Nfft : np.uintc,boxsize : np.single,dk : np.single):

        #This factor appears due to the Dirac delta
        factor=1/boxsize**3
       
        P0,k0=self.__spherical_average(f1_fft,f2_fft,k,Nfft,dk)
        P0*=factor
        
        #This is set by the integral constraint
        P0[0]=0.

        if self._is_survey==True:
            P0*=boxsize**3
        
        return P0,k0
        
    
    #Compute F(r)Y*_lm(theta,phi) of eq (2.7) https://arxiv.org/pdf/1704.02357.pdf
    def __get_F_Y_lm(self,f,Ngrid,boxsize,l,m,obs_coords):

        gridsize=boxsize/Ngrid
        Ngrid3=Ngrid**3

        F_Y=np.zeros_like(f)
        
        pos_c=koor2pos(np.arange(0,Ngrid3,1),boxsize,Ngrid,centered=True)

        _,theta_c,phi_c=cart2polar(pos_c,(obs_coords+0.5)*gridsize)

        del pos_c,_

        F_Y=f*sph_harm(m,l,theta_c,phi_c)

        return F_Y


    #Compute F_l of eq (2.7) https://arxiv.org/pdf/1704.02357.pdf
    def __get_F_l(self,f,Ngrid,boxsize,l,obs_coords,workers):

        F_l=np.zeros_like(f,dtype=np.complex128)

        gridsize=boxsize/Ngrid

        for m in np.arange(-l,l+1,1):
            
            F_Y_lm=self.__get_F_Y_lm(f,Ngrid,boxsize,l,m,obs_coords)
            F_Y_lm_fft,kx,ky,kz=self.__compute_fft(F_Y_lm,Ngrid,boxsize,workers)
            F_Y_lm_fft*=gridsize**3

            del F_Y_lm
            k=np.array([kx,ky,kz])

            _,theta_k,phi_k=cart2polar(k)
            F_l+=sph_harm(m,l,theta_k,phi_k)*F_Y_lm_fft


        F_l*=(4*math.pi)/(2*l+1)

        return F_l


    #Routine for power spectrum monopole. Computes the angular average of f1_fft*f2_fft* over spherical shells 
    #of depth dk
    @staticmethod
    @jit(nopython=True,parallel=True)
    def __spherical_average(f1_fft,f2_fft,k,Ngrid,dk):
        
        Ngrid3=Ngrid**3
        kmax=np.max(np.sqrt(k.T[0]**2+k.T[1]**2+k.T[2]**2))
        lmax=np.uintc(math.ceil(kmax/dk))
        
        F0=np.zeros(lmax,dtype=np.complex128)
        kF0=np.zeros(lmax,dtype=np.single)
        counts=np.zeros(lmax,dtype=np.uintc)
        
        #How we account for parallelization?
        for i in prange(Ngrid3):
            k_abs=math.sqrt(k[i][0]**2+k[i][1]**2+k[i][2]**2)
            l=np.uintc(math.floor(k_abs/dk+0.5))
            if l<lmax:
                d_fft_norm_real=f1_fft[i].real*f2_fft[i].real+f1_fft[i].imag*f2_fft[i].imag
                d_fft_norm_imag=-f1_fft[i].real*f2_fft[i].imag+f1_fft[i].imag*f2_fft[i].real

                F0[l]+=d_fft_norm_real+d_fft_norm_imag*1j
                kF0[l]+=k_abs
                counts[l]+=1
        
        for l in range(lmax):
            if l!=0:
                F0[l]/=counts[l]
                kF0[l]/=counts[l]
        
        return F0,kF0

    
    #Spline used for the FKP estimator
    def __build_spline(self,xbin : np.ndarray,ybin : np.ndarray,bc : str = 'natural'):
    
        f=CubicSpline(xbin,ybin,bc_type=bc)
    
        return lambda x: np.where((xbin[0]<=x) & (x<=xbin[-1]),f(x),0.)

    #Fast way to plot a field
    def __plot_field(self,field,Ngrid,axis=0,height=None,Navg=None,clim=None,logScale=False,cbar=True,title="SET_TITLE",
               filename=None,return_object=False):
    
        Ngrid3=Ngrid**3
        
        if field.shape==(Ngrid,Ngrid,Ngrid):
            F=np.copy(field)
        elif len(field)==Ngrid3:
            F=field.reshape(Ngrid,Ngrid,Ngrid)
        else:
            print('The input is not in a recognizable shape.', file=sys.stderr)
            sys.exit()
            
        if height is None:
            height=int(Ngrid/2)
        
        if Navg is None:
            Navg=int(Ngrid/20)
            
    
        if Navg>0:
            if axis==0:
                slic=np.mean(F[height-Navg:height+Navg,:,:],axis=0)
            elif axis==1:
                slic=np.mean(F[:,height-Navg:height+Navg,:],axis=1)
            elif axis==2:
                slic=np.mean(F[:,:,height-Navg:height+Navg],axis=2)
        else:
            if axis==0:
                slic=F[height,:,:]
            elif axis==1:
                slic=F[:,height,:]
            elif axis==2:
                slic=F[:,:,height]
        
        if logScale==False:
            fg=plt.imshow(slic)
        else:
            idx=np.where(slic==0.)
            slic[idx]=1.
            fg=plt.imshow(slic,norm=LogNorm())

        if cbar==True:
            plt.colorbar(fg)
        
        if clim is not None:
            plt.clim(clim[0],clim[1])
            
        plt.title(title)
        out=plt.gca()

        if filename is not None:
            plt.savefig(filename)
        
        if return_object==True:
            return out

    
        
    #Public methods---------------------------------------------------------------------------
    
    def load_field(self,field,Ngrid,boxsize,cshape):
        
        self.__load_field(field,Ngrid,boxsize,cshape)
        
        
    def build_from_particles(self,pos,Ngrid,boxsize,cshape,centered=False):
        
        self.__build_n_from_particles(pos,Ngrid,boxsize,cshape,centered)


    def build_FKP_from_particles(self,pos,Ngrid,boxsize,cshape,weight,rmin,rmax,Nbins,mask):
        
        self.__build_FKP_from_particles(pos,Ngrid,boxsize,cshape,weight,rmin,rmax,Nbins,mask)

        
    #Computes all the necessary FFTs given the number of multipoles needed    
    def compute_dft(self,max_poles=0,workers=1):
        
        if self._field_loaded==False:
            print("Error! Field/s not loaded yet")
            sys.exit()

        gridsize=self._boxsize/self._Ngrid

        #For simplicity the observer is set at the center of the box, it can be generalized
        obs_pos=[self._boxsize/2.,self._boxsize/2.,self._boxsize/2.]
        obs_coords=np.round(obs_pos/self._boxsize*self._Ngrid)

        #If we are self-correlating f1=f2

        if self._is_cross==False:
        
            f_fft,kx,ky,kz=self.__compute_fft(self._field1,self._Ngrid,self._boxsize,workers)
        
            self._field1_dft=np.copy(f_fft)*gridsize**3
            self._dft_k=np.copy(np.array([kx,ky,kz]).T)

            self._poles=np.arange(1,max_poles+1,1)
            self._F1_l=np.empty(max_poles,dtype=np.ndarray)

            for i in np.arange(0,max_poles,1):
                
                self._F1_l[i]=self.__get_F_l(self._field1,self._Ngrid,self._boxsize,self._poles[i],obs_coords,workers)

            self._dft_computed=True

        else:

            f_fft,kx,ky,kz=self.__compute_fft(self._field1,self._Ngrid,self._boxsize,workers)
        
            self._field1_dft=np.copy(f_fft)*gridsize**3
            self._dft_k=np.copy(np.array([kx,ky,kz]).T)

            self._poles=np.arange(1,max_poles+1,1)
            self._F1_l=np.empty(max_poles,dtype=np.ndarray)

            for i in np.arange(0,max_poles,1):
                
                self._F1_l[i]=self.__get_F_l(self._field1,self._Ngrid,self._boxsize,self._poles[i],obs_coords,workers)


            f_fft,kx,ky,kz=self.__compute_fft(self._field2,self._Ngrid,self._boxsize,workers)
        
            self._field2_dft=np.copy(f_fft)*gridsize**3
            self._dft_k=np.copy(np.array([kx,ky,kz]).T)

            self._poles=np.arange(1,max_poles+1,1)
            self._F2_l=np.empty(max_poles,dtype=np.ndarray)

            obs_pos=[self._boxsize/2.,self._boxsize/2.,self._boxsize/2.]
            obs_coords=np.round(obs_pos/self._boxsize*self._Ngrid)

            for i in np.arange(0,max_poles,1):
                
                self._F2_l[i]=self.__get_F_l(self._field2,self._Ngrid,self._boxsize,self._poles[i],obs_coords,workers)


            self._dft_computed=True

    #The monopole procedure also applies MAS deconvolution and aliasing corrections, so it is a separate process
    def get_monopole(self,aliasing=False,dk=None):
        
        if self._dft_computed==False:
            print("Error! Dft not computed yet")
            sys.exit()
            
        #If the binning is not selected, use the fundamental frequency
        if dk is None:
            dk=2*math.pi/self._boxsize

        if self._is_cross==True:

            F1=self.__compensate_field(self._field1_dft,self._dft_k,self._Ngrid,self._boxsize,self._MAS,aliasing)
            F2=self.__compensate_field(self._field2_dft,self._dft_k,self._Ngrid,self._boxsize,self._MAS,aliasing)
            
        else:

            F1=self.__compensate_field(self._field1_dft,self._dft_k,self._Ngrid,self._boxsize,self._MAS,aliasing)
            F2=F1
        
        P0,k0=self.__compute_monopole(F1,F2,self._dft_k,self._Ngrid,self._boxsize,dk=dk)
        
        return P0,k0

    #Obtain the chosen multipole
    def get_multipole(self,l=1,dk=None):
        
        if self._dft_computed==False:
            print("Error! Dft not computed yet")
            sys.exit()
            
        if l not in self._poles:
            print("Error! Multipole non computed")
            sys.exit()

        idx_l=np.argwhere(self._poles==l)[0][0]

        #If the binning is not selected, use the fundamental frequency
        if dk is None:
            dk=2*math.pi/self._boxsize

        if self._is_cross==True:

            Pl,kl=self.__spherical_average(self._field1_dft,self._F2_l[idx_l],
            self._dft_k,self._Ngrid,dk=dk)

        else:
            Pl,kl=self.__spherical_average(self._field1_dft,self._F1_l[idx_l],
            self._dft_k,self._Ngrid,dk=dk)

    
        Pl*=(2*l+1)/(4*math.pi)
        
        return Pl,kl


    def plot(self,axis=0,height=None,Navg=None,clim=None,logScale=False,cbar=True,title="SET_TITLE",
               filename=None,return_object=False,which=1):

        if self._is_cross==False:

            if which==1:
                
                return self.__plot_field(self._field1,self._Ngrid,axis,height=height,Navg=Navg,clim=clim,
                logScale=logScale,cbar=cbar,title=title,filename=filename,return_object=return_object)

            else:
                print("Error! There is only one field to show.")
                sys.exit()

        elif self._is_cross==True:
            
            if which==1:

                return self.__plot_field(self._field1,self._Ngrid,axis,height=height,Navg=Navg,clim=clim,
                logScale=logScale,cbar=cbar,title=title,filename=filename,return_object=return_object)

            elif which==2:

                return self.__plot_field(self._field2,self._Ngrid,axis,height=height,Navg=Navg,clim=clim,
                logScale=logScale,cbar=cbar,title=title,filename=filename,return_object=return_object)
        
            else:

                print("Error! Field {} does not exist.",which)
                sys.exit()        
      
    #Retrieving methods
    
    """
    def get_field(self):
        return np.copy(self._field)
    """

    def get_Ngrid(self):
        return self._Ngrid
    
    def get_boxsize(self):
        return self._boxsize
    
    def get_MAS(self):
        return self._MAS
    
    def get_field_dft(self):
        return np.copy(self._field_dft)
    
    def get_k_dft(self):
        return np.copy(self._dft_k)

    def get_PSN(self):
        return self._PSN
    
    
#Some of these functions are outside the class to be used in numba

#Periodic value of d in a box
@jit(nopython=True)
def periDist(d : np.double,boxsize : np.double):
    
    if abs(d)>=boxsize/2.:
        return abs(d)-boxsize/2.
    else:
        return abs(d)

#Periodic gridcell in a grid
@jit(nopython=True)
def periGrid(a : np.int32,Ngrid : np.uintc):
    
    if a>=Ngrid:
        a-=Ngrid
    elif a<0:
        a+=Ngrid
        
    return a

#From a position go to the respective gridcell    
@jit(numba.types.UniTuple(numba.int32,3)(numba.float64,numba.float64,numba.float64,
                                          numba.int64,numba.uint32),nopython=True)
def pos2grid(x : np.double,y : np.double,z : np.double,boxsize : np.double,Ngrid : np.uintc):

    if x>boxsize or y>boxsize or z>boxsize:
        print('Found a particle outside the box')
        raise ValueError
        
    if x<0 or y<0 or z<0:
        print('Found a particle outside the box')
        raise ValueError
        
    H=boxsize/Ngrid
    a=np.int32(math.floor(x/H))
    b=np.int32(math.floor(y/H))
    c=np.int32(math.floor(z/H))

    #a=np.where(a==Ngrid,Ngrid-1,a)
    #b=np.where(b==Ngrid,Ngrid-1,b)
    #c=np.where(c==Ngrid,Ngrid-1,c)

    return a,b,c
    
#From a C-ordered coordinate go to the grid
def koor2grid(koor : np.ndarray,Ngrid : np.uintc,centered : bool):
        
    a=np.floor((koor.astype(np.single)/Ngrid**2)).astype(np.intc,casting="unsafe")
    b=np.floor((koor.astype(np.single)-a*Ngrid**2)/Ngrid).astype(np.intc,casting="unsafe")
    c=np.floor((koor.astype(np.single)-b*Ngrid-a*Ngrid**2)).astype(np.intc,casting="unsafe")

    if centered==True:
        Nshift=int(math.floor(Ngrid/2.))
        a-=Nshift
        b-=Nshift
        c-=Nshift
        
    return a,b,c


#From a gridcell go to the relative position in its center
def grid2pos(a : np.ndarray,b : np.ndarray,c : np.ndarray,boxsize : np.single,Ngrid : np.uintc,centered : bool): 

    H=boxsize/Ngrid
    x=(a+0.5)*H
    y=(b+0.5)*H
    z=(c+0.5)*H
    if centered==False:
        if np.any((a<0) & (a>=Ngrid)) or np.any((b<0) & (b>=Ngrid)) or np.any((b<0) & (b>=Ngrid)):
            ValueError("Found a cell outside the box")
            sys.exit()
    else:
        Nshift=int(math.floor(Ngrid/2.))
        if np.any((a<-Nshift) & (a>=Nshift)) or np.any((b<-Nshift) & (b>=Nshift)) or np.any((b<-Nshift) & (b>=Nshift)):
            ValueError("Found a cell outside the box")
            sys.exit()
  
    return x,y,z


#From a C-ordered coordinate go to its relative position
def koor2pos(koor : np.ndarray,boxsize : np.single,Ngrid : np.uintc,centered : bool): 
    a,b,c=koor2grid(koor,Ngrid,centered)
    return grid2pos(a,b,c,boxsize,Ngrid,centered)


#Get the distance of a cell given its koor, from the center of the grid
def koorDist(koor : np.ndarray,boxsize : np.single,Ngrid : np.uintc): 
    x,y,z=koor2pos(koor,boxsize,Ngrid,centered=True)
    return np.sqrt(x**2+y**2+z**2)

#Go from cartesian to polar coordinates
def cart2polar(pos,obsPos=[0,0,0]):
    
    x,y,z=np.copy(pos)
    
    x-=obsPos[0]
    y-=obsPos[1]
    z-=obsPos[2]
    
    r=np.sqrt(x**2+y**2+z**2)
    theta=np.arctan2(y,x)
    phi=np.zeros_like(theta)
    mask=r>0.
    phi[mask]=z[mask]/r[mask]
    phi[mask==False]=1
    
    return r,theta,phi


#Computes the particle weights for the CIC MAS
@jit(numba.float64(numba.float64,numba.float64,numba.float64,numba.int64,numba.int64,numba.int64,
                   numba.uint32,numba.float64),nopython=True)
def CIC_weight(x : np.double,y : np.double,z : np.double,a : np.int32,b : np.int32,c : np.int32, 
                    Ngrid : np.uintc, boxsize : np.double):
        
    H=boxsize/Ngrid
        
    w=1.
        
    w*=(1.-periDist(H*(a+0.5)-x,boxsize)/H)
    if w < 0.:
        return 0
        
    w*=(1.-periDist(H*(b+0.5)-y,boxsize)/H)
    if w < 0.:
        return 0
        
    w*=(1.-periDist(H*(c+0.5)-z,boxsize)/H)
    if w < 0.:
        return 0
    
    return w

#Save the power spectrum in a txt file 
def save_power(k : np.ndarray,P : np.ndarray,f_prefix : str):

    P_save=P.astype(np.complex128)
    k_save=k.astype(np.double)

    np.savetxt(fname=f_prefix+"_P.txt",X=P_save)
    np.savetxt(fname=f_prefix+"_k.txt",X=k_save)

#Load the power spectrum with the same prescription
def load_power(f_prefix : str):
    
    P=np.loadtxt(f_prefix+"_P.txt",dtype=np.complex_)
    k=np.loadtxt(f_prefix+"_k.txt")

    return k,P
